package com.conceptioni.login;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginActivity extends AppCompatActivity {

    EditTextExtended etpassword, etemail;
    @NonNull
    String TAG = "LoginActivity";
    ProgressBar pb;
    TextViewRegular tv_forgot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initialize();
        clicks();

    }

    private void initialize() {
        etpassword = findViewById(R.id.et_password);
        etemail = findViewById(R.id.et_email);
        pb = findViewById(R.id.pb);
        tv_forgot = findViewById(R.id.tv_forgot);
    }

    private void clicks() {
        findViewById(R.id.login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkEditText(etemail, "Enter email") && checkEditText(etpassword, "Enter password")) {
                    if (checkEmail()) {

                        Map<String, String> params = new HashMap<>();
                        params.put("API", "login_process");
                        params.put("email", etemail.getText().toString());
                        params.put("password", etpassword.getText().toString());
                        params.put("device_id", "13245");

                        Log.d(TAG, "onClick: \n" + params.toString());
                        // loginData(params);
                    }
                }

            }
        });

        findViewById(R.id.register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            }
        });

        tv_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ForgotPasswordDialogue(LoginActivity.this).showAddMeetingDialogue();
            }
        });
    }

    public class ForgotPasswordDialogue {

        Context context;
        TextViewLight tvrSave;
        Dialog dialog;
        EditTextExtended eoldpsw;

        ForgotPasswordDialogue(Context context) {
            this.context = context;
        }

        public void showAddMeetingDialogue() {
            dialog = new Dialog(context, R.style.Theme_CustomDialog);
            dialog.setContentView(R.layout.dialog_forgot);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(true);

            eoldpsw = dialog.findViewById(R.id.eoldpsw);
            tvrSave = dialog.findViewById(R.id.tvrSave);

            dialog.show();

            tvrSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (eoldpsw.getText().toString().length() > 0) {

                        String EMAIL_PATTERN =
                                "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

                        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
                        Matcher matcher = pattern.matcher(eoldpsw.getText().toString());

                        if (matcher.matches()) {
                            // CallForgotPassword(eoldpsw.getText().toString());
                            dialog.dismiss();
                        } else {
                            Toast.makeText(context, "Enter Correct Email", Toast.LENGTH_SHORT).show();
                        }


                    } else {
                        Toast.makeText(context, "Enter Email", Toast.LENGTH_SHORT).show();
                    }
                }
            });


        }
    }

    private boolean checkEditText(EditTextExtended et, String msg) {

        if (et.getText().toString().equals("")) {
            et.setError(msg);
            et.requestFocus();
            return false;
        }
        return true;
    }

    private boolean checkEmail() {
        String EMAIL_PATTERN =
                "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(etemail.getText().toString());

        if (!matcher.matches()) {
            etemail.setError("Email is not correct");
            etemail.requestFocus();
            return false;
        }


        return true;
    }

   /* public void loginData(final Map<String, String> params) {

        pb.setVisibility(View.VISIBLE);

        RequestQueue requestQueue = Volley.newRequestQueue(getApplication());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.user_login, new Response.Listener<String>() {
            @Override
            public void onResponse(@Nullable String response) {
                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        Toast.makeText(LoginActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        if (jsonObject.getString("success").equalsIgnoreCase("1")) {
                            JSONObject userdata = jsonObject.optJSONObject("userdata");
                            SharedPrefs.getSharedPref().edit().putString(SharedPrefs.userSharedPrefData.int_glcode, userdata.optString("profile_id")).apply();
                            SharedPrefs.getSharedPref().edit().putString(SharedPrefs.userSharedPrefData.email, userdata.optString("email")).apply();
                            SharedPrefs.getSharedPref().edit().putString(SharedPrefs.userSharedPrefData.matrimony_id, userdata.optString("matrimony_id")).apply();
                            SharedPrefs.getSharedPref().edit().putString(SharedPrefs.userSharedPrefData.phone, userdata.optString("phone")).apply();
                            SharedPrefs.getSharedPref().edit().putString(SharedPrefs.userSharedPrefData.profile_name, userdata.optString("profile_name")).apply();
                            SharedPrefs.getSharedPref().edit().putString(SharedPrefs.userSharedPrefData.age, userdata.optString("age")).apply();
                            SharedPrefs.getSharedPref().edit().putString(SharedPrefs.userSharedPrefData.profile_photo, userdata.optString("profile_photo")).apply();
                            SharedPrefs.getSharedPref().edit().putString(SharedPrefs.userSharedPrefData.address, userdata.optString("city_name") + "," + userdata.optString("state_name")).apply();

                            SharedPrefs.getSharedPref().edit().putString(SharedPrefs.userSharedPrefData.userdata, userdata.toString()).apply();
                            startActivity(new Intent(LoginActivity.this, HomeActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                        } else {
                            if (jsonObject.getString("message").equalsIgnoreCase("Your Profile is not yet Approved.")) {
                                startActivity(new Intent(LoginActivity.this, ApprovalScreenActivity.class));
                            } else if (jsonObject.getString("message").equalsIgnoreCase("Your Phone is not Verify.")) {
                                startActivity(new Intent(LoginActivity.this, OtpActivity.class).putExtra("number", jsonObject.getString("phone")));
                            }
                        }

                    } catch (JSONException e) {

                        e.printStackTrace();
                    }
                }

                pb.setVisibility(View.GONE);

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  new MakeToast("Could not able to login due to slow internet connectivity. Please try after some time");
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                return params;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    public void CallForgotPassword(final String opsw) {
        RequestQueue requestQueue = Volley.newRequestQueue(getApplication());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.forgotPassword, new Response.Listener<String>() {
            @Override
            public void onResponse(@Nullable String response) {
                if (response != null) {
                    try {
                        Log.d("++++loginjson", "++ " + response);
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("success").equalsIgnoreCase("1")) {

                            Toast.makeText(LoginActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        } else {
                            Log.d("+++1", "++1 ");
                            Toast.makeText(LoginActivity.this, jsonObject.getString("userdata"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // new MakeToast("Could not able to login due to slow internet connectivity. Please try after some time");
                    }
                }) {
            @NonNull
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("API", "forgot_password");
                params.put("email", opsw);

                Log.d("++++paramLogin", "+++++" + params.toString());
                return params;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }*/
}
